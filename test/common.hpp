/*
 * common.h:
 *
 * Author(s): objectx
 *
 */
#pragma once
#ifndef common_hpp__280f1ce8_a4b7_4420_b827_c6b9c4f563d8
#define common_hpp__280f1ce8_a4b7_4420_b827_c6b9c4f563d8  1

#if defined (_WIN32) || defined (_WIN64)
#define WIN32_LEAN_AND_MEAN     1
#include <windows.h>
#endif

#include <sys/types.h>
#include <assert.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cctype>
#include <algorithm>
#include <iterator>

#endif  /* common_hpp__280f1ce8_a4b7_4420_b827_c6b9c4f563d8 */
/*
 * $LastChangedRevision$
 * $LastChangedBy$
 * $HeadURL$
 */
